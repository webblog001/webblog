﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using WebBlog.Models;

namespace WebBlog.BlogIslem
{
    public class blogContext : DbContext
    {
        public blogContext():base("blogContext") { }
        
        public DbSet<Ayarlar> Ayarlar { get; set; }
        public DbSet<Kullanici> Kullanicilar { get; set; }
        public DbSet<Yazi> Yazilar { get; set; }
        public DbSet<Yorum> Yorumlar { get; set; }
        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}