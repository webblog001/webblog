﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebBlog.Models;

namespace WebBlog.BlogIslem
{
    public class blogBaslangicData : System.Data.Entity.DropCreateDatabaseIfModelChanges<blogContext>
    {
        protected override void Seed(blogContext context)
        {
            var Yazilar = new List<Yazi>
            {
                new Yazi { YaziID=1,YaziBaslik="asd",
                    YaziIcerik = "qwerwqe",
                    YayinciAdi = "yayinciadi",
                    YayinTarihi = DateTime.Parse("25-04-2015") },
                new Yazi { YaziID=2,YaziBaslik="qwe",
                    YaziIcerik = "qwerwqasdasde",
                    YayinciAdi = "yayinciadasdasdi",
                    YayinTarihi = DateTime.Parse("27-04-2015") }
            
        };
            Yazilar.ForEach(s => context.Yazilar.Add(s));
            context.SaveChanges();

            var Yorumlar = new List<Yorum>
            {
                new Yorum { YorumID=1,
                    YorumIcerik ="asdasd",
                    YorumlayanAdi ="asdasd",
                    YaziID =1 },
                new Yorum { YorumID=2,
                    YorumIcerik ="asdaasd sd",
                    YorumlayanAdi ="asasd dasd",
                    YaziID =1 }
            };
            Yorumlar.ForEach(s => context.Yorumlar.Add(s));
            context.SaveChanges();

            var Kullanicilar = new List<Kullanici>
            {
                new Kullanici { KullaniciID=1,KullaniciAdi="admin",KullaniciAdSoyad="asd",KullaniciMail="asd@asd.com",KullaniciSifre="asdasd",UyelikDurumu=1 }
              
            };
            Kullanicilar.ForEach(s => context.Kullanicilar.Add(s));
            context.SaveChanges();

            var Ayarlar = new List<Ayarlar>
            {
                new Ayarlar { AyarlarID=1, SiteBasligi="asdasd",SiteSlogani="asdasd",SiteMail="asdasd@aads.com",SiteAdres="asdasd" }

            };
            Ayarlar.ForEach(s => context.Ayarlar.Add(s));
            context.SaveChanges();
        }

    }
}