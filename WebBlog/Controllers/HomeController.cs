﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBlog.BlogIslem;
using WebBlog.Models;

namespace WebBlog.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Yazis");
        }
       

        public ActionResult Hakkinda()
        {

            return View();
        }

        public ActionResult Iletisim()
        {

            return View();
        }

        public ActionResult Giris()
        {
            return RedirectToAction("Giris", "Kullanicis");
        }

        public ActionResult Kayitol()
        {
            return RedirectToAction("Kayitol", "Kullanicis");
        }

    }
}