﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebBlog.BlogIslem;
using WebBlog.Models;

namespace WebBlog.Controllers
{
    public class YazisController : Controller
    {
        private blogContext db = new blogContext();

        // GET: Yazis
        public ActionResult Index()
        {
            return View(db.Yazilar.ToList());
        }

        // GET: Yazis/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yazi yazi = db.Yazilar.Find(id);
            if (yazi == null)
            {
                return HttpNotFound();
            }
            return View(yazi);
        }

        // GET: Yazis/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Yazis/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "YaziID,YaziBaslik,YaziIcerik,YayinciAdi,YayinTarihi")] Yazi yazi)
        {
            if (ModelState.IsValid)
            {
                db.Yazilar.Add(yazi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(yazi);
        }

        // GET: Yazis/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yazi yazi = db.Yazilar.Find(id);
            if (yazi == null)
            {
                return HttpNotFound();
            }
            return View(yazi);
        }

        // POST: Yazis/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "YaziID,YaziBaslik,YaziIcerik,YayinciAdi,YayinTarihi")] Yazi yazi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(yazi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(yazi);
        }

        // GET: Yazis/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yazi yazi = db.Yazilar.Find(id);
            if (yazi == null)
            {
                return HttpNotFound();
            }
            return View(yazi);
        }

        // POST: Yazis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Yazi yazi = db.Yazilar.Find(id);
            db.Yazilar.Remove(yazi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



        public ActionResult Hakkinda()
        {

            return View();
        }

        public ActionResult Iletisim()
        {

            return View();
        }

        public ActionResult indexyaziAdmin()
        {

            return View();
        }

        public ActionResult Giris()
        {
            return View("~/Views/Kullanicis/Giris.cshtml");
        }

        public ActionResult Kayitol()
        {
            return View("~/Views/Kullanicis/Kayitol.cshtml");
        }

        public ActionResult AdminPanel()
        {

            return View("~/Views/Manage/IndexAdmin.cshtml");
        }
    }
}
