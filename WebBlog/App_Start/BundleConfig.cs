﻿using System.Web;
using System.Web.Optimization;

namespace WebBlog
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            
            //anasayfa icin
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/all-stylesheets.css"));
        
            bundles.Add(new ScriptBundle("~/Javascripts").Include(
                        "~/Scripts/js/jquery.min.js",
                        "~/Scripts/js/bootstrap/bootstrap.min.js",
                        "~/Scripts/js/jquery.easing.min.js",
                        "~/owl-carousel/js/owl.carousel.js",
                        "~/Scripts/js/jquery.prettyPhoto.js",
                        "~/Scripts/js/jquery.jigowatt.js",
                        "~/Scripts/js/custom.js"));


            
            //admin sayfasi icin
            bundles.Add(new StyleBundle("~/Admin/css").Include(
                      "~/Content/admin/css/bootstrap.min.css",
                      "~/Content/admin/assets/font-awesome/css/font-awesome.min.css",
                      "~/Content/admin/assets/ionicon/css/ionicons.min.css",
                      "~/Content/admin/css/material-design-iconic-font.min.css",
                      "~/Content/admin/css/animate.css",
                      "~/Content/admin/css/waves-effect.css",
                      "~/Content/admin/assets/sweet-alert/sweet-alert.min.css",
                      "~/Content/admin/css/helper.css",
                      "~/Content/admin/css/style.css"));

            bundles.Add(new ScriptBundle("~/Admin/Javascripts").Include(
                        "~/Scripts/jsAdmin/modernizr.min.js",
                        "~/Scripts/jsAdmin/jquery.min.js",
                        "~/Scripts/jsAdmin/bootstrap.min.js",
                        "~/Scripts/jsAdmin/waves.js",
                        "~/Scripts/jsAdmin/wow.min.js",
                        "~/Scripts/jsAdmin/jquery.nicescroll.js",
                        "~/Scripts/jsAdmin/jquery.scrollTo.min.js",
                        "~/Content/admin/assets/chat/moment-2.2.1.js",
                        "~/Content/admin/assets/jquery-sparkline/jquery.sparkline.min.js",
                        "~/Content/admin/assets/jquery-detectmobile/detect.js",
                        "~/Content/admin/assets/fastclick/fastclick.js",
                        "~/Content/admin/assets/jquery-slimscroll/jquery.slimscroll.js",
                        "~/Content/admin/assets/jquery-blockui/jquery.blockUI.js",
                        "~/Content/admin/assets/sweet-alert/sweet-alert.min.js",
                        "~/Content/admin/assets/sweet-alert/sweet-alert.init.js",
                        "~/Content/admin/assets/flot-chart/jquery.flot.js",
                        "~/Content/admin/assets/flot-chart/jquery.flot.time.js",
                        "~/Content/admin/assets/flot-chart/jquery.flot.tooltip.min.js",
                        "~/Content/admin/assets/flot-chart/jquery.flot.resize.js",
                        "~/Content/admin/assets/flot-chart/jquery.flot.pie.js",
                        "~/Content/admin/assets/flot-chart/jquery.flot.selection.js",
                        "~/Content/admin/assets/flot-chart/jquery.flot.stack.js",
                        "~/Content/admin/assets/flot-chart/jquery.flot.crosshair.js",
                        "~/Content/admin/assets/counterup/waypoints.min.js",
                        "~/Content/admin/assets/counterup/jquery.counterup.min.js",
                        "~/Scripts/jsAdmin/jquery.app.js",
                        "~/Scripts/jsAdmin/jquery.dashboard.js",
                        "~/Scripts/jsAdmin/jquery.chat.js",
                        "~/Scripts/jsAdmin/jquery.todo.js"));



        }
    }
}
