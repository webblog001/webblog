﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBlog.Models
{
    public class Yorum
    {
        public int YorumID { get; set; }
        public String YorumIcerik { get; set; }
        public String YorumlayanAdi { get; set; }
        public int YaziID { get; set; }

        // public virtual ICollection<Yazi> Yazilar { get; set; }


    }
}