﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBlog.Models
{
    public class Kullanici
    {

        public int KullaniciID { get; set; }
        public String KullaniciAdi { get; set; }
        public String KullaniciSifre { get; set; }
        public String KullaniciMail { get; set; }
        public String KullaniciAdSoyad { get; set; }
        public int UyelikDurumu { get; set; }
        

    }
}