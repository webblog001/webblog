﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBlog.Models
{
    public class Yazi
    {
        public int YaziID { get; set; }
        public String YaziBaslik { get; set; }
        public String YaziIcerik { get; set; }
        public String YayinciAdi { get; set; }
        public DateTime YayinTarihi {get; set;}

        // public virtual Yorum Yorumlar { get; set; }
        
    }
}